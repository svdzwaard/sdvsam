#' Summarise segment data of passings within a speed skating session
#'
#' Using your summarise Mylaps segment data from the database of the Dutch Royal Speed Skating Federation (KNSB) in Thialf,
#'
#' @param segment_data dataframe with segment data obtained by retrieve_segment_passings
#'
#' @return summarised Mylaps data from the KNSB Mylaps database in Thialf
#' @export
#'
#' @author Stephan van der Zwaard, \email{stephan@@sportdatavalley.nl}
#' @keywords Preprocessing, Segment Analysis Mylaps
#'
#' @examples
#' # summarise_segment_data(segment_data)
#'
#' @importFrom stats setNames
#'

summarise_segment_data <- function(segment_data) {

  # Transform data to Sprintcoach format
  options(digits.secs = 3)

  # Processing of Mylaps data
  summarised_data <- segment_data %>%
                     filter(chip == chip[1]) %>%
                     summarise(date                  = date[1],
                               rider                 = rider_id[1],
                               ice_start             = ymd_hms(min(start)),
                               ice_duration_min      = round(difftime(max(end),min(start),units = "mins")),
                               ice_distance_km       = sum(split_dist_IN_border,na.rm=T)/1000,
                               ice_nr_laps           = length(unique(lap_id)),
                               ice_fastest_lap_s     = min(lap_time_s),
                               ice_mean_speed_kmh    = mean(split_speed_IN_border,na.rm=T),
                               ice_max_speed_segment = max(split_speed_IN_border,na.rm=T),
                               ice_max_speed_lap     = max(ice_fastest_400m_speed, na.rm=T),
                               ice_max_speed_finish  = paste0("L",loop_id_end[which.max(ice_fastest_400m_speed)]),
                               ice_max_accel_segment = max(split_accel_IN_border,na.rm=T))

  return(summarised_data)

}
